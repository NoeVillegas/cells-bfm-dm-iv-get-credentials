class CellsBfmDmIvGetCredentials extends Polymer.Element {

  static get is() {
    return 'cells-bfm-dm-iv-get-credentials';
  }

  static get properties() {
    return {
      endpoint: String,
      headers: {
        type: String,
        value: () => ({})
      },
      host: {
        type: String,
        value: () => ''
      },
      isFirts:{
        type: Boolean,
        value: true
      }
    };
  }
  /**
   * Function to handle service success response
   * @param {*} response
   */
  _successResponse(response) {
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    if (response.detail.response.length) {
      this.dispatchEvent(new CustomEvent('has-credentials',{
        detail:response.detail.response[0].id_credential
      }))
    }

    if (!response.detail.response.length) {
      this.dispatchEvent(new CustomEvent('missing-credentials'));
    }

    this.dispatchEvent(new CustomEvent('full-data-credentials', {detail: response.detail.response}));
  }
  /**
   * Function to generate enpoint and call service
   */
  starterMethod(evt) {
    //This evt.value come from private channel hook to validate if generate request only when page is active.
    let entryPage = evt.value;
    this._buildEndpoint();
    if (this.endpoint && entryPage) {
      this.$.dpGetCredentials.generateRequest();
    }
  }
  /**
   * function to buil enpoint url
   */
  _buildEndpoint() {
    let host = this._getHost();
    let isMock = this._getMocks();
    //Rmo
    if(this.isFirts){
      this.set('endpoint', `${host}/credentials${isMock}`);
      this.isFirts = false;
    } else{
      this.set('endpoint', `${host}/credentials_2${isMock}`);
    }
  }
  _getMocks(){
    return (window.AppConfig && window.AppConfig.mocks) ? '.json' : '';
  }
  /**
   * Function to get host from config file
   */
  _getHost() {
    return (window.AppConfig && window.AppConfig.host) ? window.AppConfig.host : this.host;
  }
  /**
   * Function to hanlde error status response
   * @param {*} reponse
   */
  _handlerError(reponse) {
    var msj = 'Estamos teniendo dificultades técnicas, favor de intentar mas tarde';
    try {
      var errorCode = reponse.detail.code;
      switch (errorCode) {
        case 401:
          this.dispatchEvent(new CustomEvent('unauthorized-error', { detail: reponse.detail.code }));
          break;
        default:
          msj = reponse.detail['error-message'] || errorCode || msj;
          break;
      }
    } catch (e) {
      //No aplicable catch handler, use default values from eventError and msj
    }
    this.dispatchEvent(new CustomEvent('account-detail', {detail: []}));
    this.dispatchEvent(new CustomEvent('error-service', { detail: msj }));
  }
}

customElements.define(CellsBfmDmIvGetCredentials.is, CellsBfmDmIvGetCredentials);
